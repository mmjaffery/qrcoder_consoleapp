﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QRCoder_ConsoleApp
{
    public static class CSVReader
    {
        internal static List<string> ReadCsv(string fileLocation)
        {
            List<string> listA = new List<string>();
            using (var reader = new StreamReader(fileLocation))
            {
                int i = 1;
                //List<string> listB = new List<string>();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    //var values = line.Split(',');
                    
                    //send to everyone
                    listA.Add(i + "_" + line);
                    
                    //send to unique members
                    //listA.Add(line);

                    //listA.Add(values[0]);
                    //listB.Add(values[1]);
                    i++;
                }
            }
            return listA;
        }
    }


}
