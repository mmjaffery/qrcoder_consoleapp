﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace QRCoder_ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileLocation = @"C:\Users\Mohammad\Downloads\bitmap_dump\Mar8.csv";
            List<string> listOfStrings = CSVReader.ReadCsv(fileLocation);

            List<string> distinctListOfStrings = listOfStrings.Distinct().ToList();

            foreach (var item in distinctListOfStrings)
            {
                GenerateQRCode(item.ToString(), 3, Color.Black, Color.White, @"C:\Users\Mohammad\Downloads\bitmap_dump\logo.jpg", @"C:\Users\Mohammad\Downloads\bitmap_dump\");
            }
        }

        private static void GenerateQRCode(string encodingString, int size, Color colorDark, Color colorLight, string logoPath, string saveLoc)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(encodingString, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            //Bitmap qrCodeImage = qrCode.GetGraphic(20);
            //Bitmap qrCodeImage = qrCode.GetGraphic(20, Color.DarkRed, Color.PaleGreen, true);
            //Bitmap qrCodeImage = qrCode.GetGraphic(20, Color.Black, Color.White, (Bitmap)Bitmap.FromFile(@"C:\Users\Mohammad\Downloads\bitmap_dump\logo.jpg"));
            Bitmap qrCodeImage = qrCode.GetGraphic(size, colorDark , colorLight , (Bitmap)Bitmap.FromFile(logoPath));


            string trimmed = String.Concat(encodingString.Where(c => !Char.IsWhiteSpace(c)));
            string illegal = trimmed;
            string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            foreach (char c in invalid)
            {
                illegal = illegal.Replace(c.ToString(), "");
            }
            qrCodeImage.Save(saveLoc + illegal + ".jpg");
        }
    }
}
